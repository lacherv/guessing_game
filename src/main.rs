use std::io;

/*
  1. The user guess a number
  2. The system generates a number between 1 and 100.
  3. The system make a serie of the check
    3.a if the guess number less than the generate number, the system ouput the message and back to 1.
    3.b if the guess number greater than the generate number, the system ouput another message and back to 1.
    3.c if the guess number is equal at the generate number, the system output the another message and end the program. 
*/

fn main() {
    println!("Guess the number!");

    println!("Please input your guess. ");

    let mut guess = String::new();

    io::stdin()
      .read_line(&mut guess)
      .expect("Failed to read line");

    println!("You guessed: {guess}");
}
